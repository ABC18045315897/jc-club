package com.jingdianjichi.auth.application.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色权限关联表(AuthRolePermission)实体类
 *
 * @since 2024-04-11 16:07:58
 */
@Data
public class AuthRolePermissionDTO implements Serializable {

    private Long id;
    /**
     * 角色id
     */
    private Long roleId;
    /**
     * 权限id
     */
    private Long permissionId;

    /**
     * 权限列表
     */
    private List<Long> permissionIdList;

}

