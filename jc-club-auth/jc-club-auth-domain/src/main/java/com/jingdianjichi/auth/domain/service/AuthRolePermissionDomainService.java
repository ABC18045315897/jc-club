package com.jingdianjichi.auth.domain.service;

import com.jingdianjichi.auth.domain.entity.AuthPermissionBO;
import com.jingdianjichi.auth.domain.entity.AuthRolePermissionBO;

/**
 * 角色权限领域service
 */
public interface AuthRolePermissionDomainService {


    Boolean add(AuthRolePermissionBO authRolePermissionBO);

}
