package com.jingdianjichi.auth.domain.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色bo
 *
 * @since 2024-04-10 15:49:15
 */
@Data
public class AuthRoleBO implements Serializable {
    private static final long serialVersionUID = 138117747211189409L;

    private Long id;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色唯一标识
     */
    private String roleKey;


}

