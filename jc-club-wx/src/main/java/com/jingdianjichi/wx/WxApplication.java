package com.jingdianjichi.wx;

/**
 * @author xyk的电脑
 * @version 1.0
 * @description: TODO
 * @date 2024/2/25 10:33
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 微信服务启动类
 */
@SpringBootApplication
@ComponentScan("com.jingdianjichi")
public class WxApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxApplication.class);
    }
}
