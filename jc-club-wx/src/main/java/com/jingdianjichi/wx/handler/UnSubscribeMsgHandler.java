package com.jingdianjichi.wx.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author xyk的电脑
 * @version 1.0
 * @description: TODO
 * @date 2024/4/14 15:18
 */
@Component
@Slf4j
public class UnSubscribeMsgHandler implements WxChatMsgHandler {
    @Override
    public WxChatMsgTypeEnum getMsgType() {
        return WxChatMsgTypeEnum.UN_SUBSCRIBE;
    }

    @Override
    public String dealMsg(Map<String, String> messageMap) {
        log.info("用户取消关注事件!");
        return null;
    }
}
