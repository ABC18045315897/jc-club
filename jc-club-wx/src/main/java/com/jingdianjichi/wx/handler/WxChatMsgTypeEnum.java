package com.jingdianjichi.wx.handler;

/**
 * @author xyk的电脑
 * @version 1.0
 * @date 2024/4/13 12:34
 */
public enum WxChatMsgTypeEnum {
    SUBSCRIBE("event.subscribe", "用户关注事件"),
    UN_SUBSCRIBE("event.unsubscribe","用户取消关注事件"),
    TEXT_MSG("text", "接收用户文本消息");


    private String msgType;

    private String desc;

    WxChatMsgTypeEnum(String msgType, String desc) {
        this.msgType = msgType;
        this.desc = desc;
    }

    public static WxChatMsgTypeEnum getByMsgType(String msgType) {
        for (WxChatMsgTypeEnum wxChatMsgTypeEnum : WxChatMsgTypeEnum.values()) {
            if (wxChatMsgTypeEnum.msgType.equals(msgType)) {
                return wxChatMsgTypeEnum;
            }
        }
        return null;
    }


}

