package com.jingdianjichi.subject.infra.basic.service;

public interface SubjectEsService {

    void createIndex();

    void addDoc();

    void find();

    void search();

}
