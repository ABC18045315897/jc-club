package com.jingdianjichi.subject.infra.entity;

import lombok.Data;

/**
 * @author xyk的电脑
 * @version 1.0
 * @description: TODO
 * @date 2024/5/9 15:30
 */
@Data
public class UserInfo {

    private String userName;

    private String nickName;

}
