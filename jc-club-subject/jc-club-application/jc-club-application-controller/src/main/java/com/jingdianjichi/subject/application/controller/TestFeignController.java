package com.jingdianjichi.subject.application.controller;


import com.jingdianjichi.subject.infra.basic.service.SubjectEsService;
import com.jingdianjichi.subject.infra.entity.UserInfo;
import com.jingdianjichi.subject.infra.rpc.UserRpc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 分类controller
 *
 * @version 1.0
 * @description: TODO
 * @date 2024/2/25 22:43
 */
@RestController
@RequestMapping("/subject/category")
@Slf4j
public class TestFeignController {

    @Resource
    private UserRpc userRpc;

    @Resource
    private SubjectEsService subjectEsService;

    @GetMapping("testFeign")
    public void testFeign() {
        UserInfo userInfo = userRpc.getUserInfo("jichi");
        log.info("testFeign.userInfo:{}", userInfo);
    }

    @GetMapping("testCreateIndex")
    public void testCreateIndex() {
        subjectEsService.createIndex();
    }

    @GetMapping("addDoc")
    public void addDoc() {
        subjectEsService.addDoc();
    }

    @GetMapping("find")
    public void find() {
        subjectEsService.find();
    }

    @GetMapping("search")
    public void search() {
        subjectEsService.search();
    }

}
