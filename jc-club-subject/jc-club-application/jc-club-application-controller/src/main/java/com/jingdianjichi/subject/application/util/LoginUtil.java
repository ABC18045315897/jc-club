package com.jingdianjichi.subject.application.util;

import com.jingdianjichi.subject.application.context.LoginContextHolder;

/**
 * 用户登录util
 *
 * @author xyk的电脑
 * @date 2024/5/9 11:20
 */
public class LoginUtil {

    public static String getLoginId(){
        return LoginContextHolder.getLoginId();
    }

}
