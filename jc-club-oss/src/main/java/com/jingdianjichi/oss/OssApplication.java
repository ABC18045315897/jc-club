package com.jingdianjichi.oss;

/**
 * @author xyk的电脑
 * @version 1.0
 * @description: TODO
 * @date 2024/2/25 10:33
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * oss服务启动类
 */
@SpringBootApplication
@ComponentScan("com.jingdianjichi")
public class OssApplication {

    public static void main(String[] args) {
        SpringApplication.run(OssApplication.class);
    }
}
