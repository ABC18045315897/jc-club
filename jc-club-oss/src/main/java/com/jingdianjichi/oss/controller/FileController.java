package com.jingdianjichi.oss.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.jingdianjichi.oss.service.FileService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author xyk的电脑
 * @version 1.0
 * @description: TODO
 * @date 2024/3/15 17:55
 */

@RestController
public class FileController {

    @Resource
    private FileService fileService;

//    @NacosValue(value = "${storage.service.type}",autoRefreshed = true)
//    private String storageType;

    @RequestMapping("/testGetAllBuckets")
    public String testGetAllBuckets() throws Exception {
        List<String> allBucket = fileService.getAllBucket();
        return allBucket.get(0);
    }

//    @RequestMapping("/testNacos")
//    public String testNacos() throws Exception {
//        return storageType;
//    }

    /**
     * 上传文件
     */
    @RequestMapping("/upload")
    public String upload(MultipartFile uploadFile, String bucket, String objectName) {
        objectName = objectName + "/" + uploadFile.getOriginalFilename();
        return fileService.uploadFile(uploadFile, bucket, objectName);
    }

}
