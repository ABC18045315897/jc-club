package com.jingdianjichi.oss.entity;

import lombok.Data;

/**
 * 文件类
 *
 * @author xyk的电脑
 * @description: TODO
 * @date 2024/3/15 17:02
 */
@Data
public class FileInfo {

    private String fileName;

    private Boolean directoryFlag;

    private String etag;

}
