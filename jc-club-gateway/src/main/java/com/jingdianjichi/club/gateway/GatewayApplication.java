package com.jingdianjichi.club.gateway;

/**
 * @author xyk的电脑
 * @version 1.0
 * @description: TODO
 * @date 2024/2/25 10:33
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 刷题微服务启动类
 */
@SpringBootApplication
@ComponentScan("com.jingdianjichi")
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class);
    }
}
